# EMS logging

# Setup
## Installation
To ensure that you get the latest version of _ems-logging_ from our repo, please add the public repo to you pip indexer with the following addition to you [pip config](https://pip.pypa.io/en/stable/user_guide/#config-file).
```
# Public repository
extra-index-url = https://gitlab.com/api/v4/projects/20076287/packages/pypi/simple
```

Now you can either install the module by `pip install ems-logging` or alternatively directly from the repository for e.g. testing an experimental branch with:
```
pip install git+https://gitlab.com/thedirtyfew/utilities/ems_logging.git
```

## Import
### Stable release
Install the package as in the installation and simply `import ems_logging`, see [Usage and configuration](#Usage-and-configuration).

### Development
Alternatively if you need to develop on this package while integrating with the package you can submodule the package with 
```
git submodule add git@gitlab.com:thedirtyfew/utilities/ems_logging.git
```
##### Note
Please checkout a new branch (`git checkout -b <dev branch name>`)

# Usage and configuration
Use the package in the follwing way:
```Python3
import ems_logging
import logging
logger = logging.getLogger("Zipping")
```

Log level can be set with env variable `EMS_LOG_LEVEL` using the python logging library levels, e.g. `INFO`, `DEBUG`, etc.

# SPAM level
An extra level below `debug` has been added, which is called `spam`. It can simply be used as the others
```
# Set level
logging.setLevel("SPAM")
# or
logging.setLevel(emslogging.SPAM)

# Do spam
logging.spam("Very spammy message")
```