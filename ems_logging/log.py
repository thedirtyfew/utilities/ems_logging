import logging
import sys
import time
import os

SPAM = 9


def default_logging_config():

    logging.basicConfig(
        format='%(asctime)s.%(msecs)03d %(levelname)-8s %(message)s',
        level=os.environ.get("EMS_LOG_LEVEL", "INFO"),
        datefmt='%Y-%m-%d %H:%M:%S',
        stream=sys.stdout
    )

    logging.Formatter.converter = time.gmtime

    # Add SPAM level
    logging.addLevelName(SPAM, "SPAM")

    def spam(self, message, *args, **kws):
        if self.isEnabledFor(SPAM):
            self._log(SPAM, message, args, **kws)

    logging.Logger.spam = spam


# Inspiration: https://stackoverflow.com/questions/54036637/python-how-to-set-logging-level-for-all-loggers-to-info
def set_all_loggers_level(loglevel):
    loggers = [logging.getLogger(name) for name in logging.root.manager.loggerDict] + [logging.getLogger()]
    for logger in loggers:
        logger.setLevel(loglevel)
